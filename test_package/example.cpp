#include <iostream>

#include "Python.h"


int main([[maybe_unused]] int argc, [[maybe_unused]] const char** argv) {
    Py_NoSiteFlag            = 1;
    Py_VerboseFlag           = 1;
    Py_IgnoreEnvironmentFlag = 1;
    Py_NoUserSiteDirectory   = 1;

    Py_InitializeEx(0);

    return 0;
}
