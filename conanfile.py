from conans import ConanFile, CMake, tools

class LibpythonConan(ConanFile):
    name            = "libpython"
    version         = "3.6.7"
    license         = "PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2"
    author          = "toge toge.mail@gmail.com"
    url             = "https://github.com/python/cpython"
    description     = "The Python programming language "
    topics          = ("python", "libpython")
    settings        = "os", "compiler", "build_type", "arch"
    generators      = "cmake"
    requires        = [
        "zlib/[>= 1.2.8]",
        "bzip2/[>= 1.0.8]",
        "openssl/[>= 1.1.1g]",
        "mpdecimal/2.4.2@bincrafters/stable",
        "expat/2.2.7",
    ]

    def source(self):
        self.run("git clone https://github.com/python-cmake-buildsystem/python-cmake-buildsystem")
        tools.replace_in_file("python-cmake-buildsystem/CMakeLists.txt", "project(Python C ASM)",
                              '''project(Python C ASM)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="python-cmake-buildsystem")
        cmake.verbose = True
        cmake.definitions["PYTHON_VERSION"] = self.version
        cmake.definitions["BUILD_STATIC"] = True
        cmake.definitions["BUILD_SHARED"] = False
        cmake.definitions["BUILD_LIBPYTHON_SHARED"] = False
        cmake.definitions["WITH_STATIC_DEPENDENCIES"] = True # Linux only
        cmake.definitions["BUILD_EXTENSIONS_AS_BUILTIN"] = True
        cmake.definitions["USE_SYSTEM_LIBRARIES"] = True
        cmake.definitions["ENABLE_HASHLIB"] = True
        cmake.definitions["BUILTIN_HASHLB"] = True
        cmake.build(target="libpython-static")
        # cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="../Python-{}/Include".format(self.version), keep_path=False)
        self.copy("pyconfig.h", dst="include", src="bin", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.py", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libs = ["python3.6m", "pthread", "dl", "util"]
